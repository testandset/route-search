package com.goeuro.domain;

/**
 * Class representing a bus station object. Its contains two bits of information that are used in BFS search.
 * One is obviously the id of the station and other the connecting bus it with another station.
 * 
 * BFS used the this connecting bus id to filter the child nodes if they should be traversed.
 * 
 * @author Deepak
 *
 */
public class Station {
	
	private int id;
	
	private int connectingBus;

	public Station(int id, int bus) {
		this.id = id;
		this.connectingBus = bus;
	}
	
	public Station(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getConnectingBus() {
		return this.connectingBus;
	}
	
	public boolean equals(Object another) {
		if(another == null) return false;
		
		return ((Station)another).getId() == this.id && 
				((Station)another).getConnectingBus() == this.getConnectingBus();
	}
	
	public int hashCode() {
		return this.id;
	}
	
	public String toString() {
		return "[" + id + ":" + connectingBus + "]";
	}
}