package com.goeuro.coreservice.finder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Naive implementation of route finder. Goes through all the lists to check if the 
 * two stations are in there. Uses Java 8's parallel stream to do it so could be quite fast to 
 * do the search.
 * 
 * Time complexity is however O(b * s) where b is number of stations and s is number of stations. * 
 * 
 * @author Deepak.Singh
 *
 */
public class NaiveRouteFinder implements RouteFinder {

	List<List<Integer>> routes = new ArrayList<List<Integer>>();
	
	@Override
	public boolean isDirectlyConnected(String departure, String arrival) {
		int dep = Integer.parseInt(departure);
		int arr = Integer.parseInt(arrival);
		
		boolean found =  routes.parallelStream()
			.anyMatch(l -> l.contains(dep) && l.contains(arr) && l.indexOf(dep) < l.indexOf(arr));
		
		return found;
		
	}

	/**
	 * Add routes to our route engine
	 * @param routeEntry
	 */
	public void addRoute(String routeEntry) {

		List<String> routeData = Arrays.asList(routeEntry.split("\\s+"));
		List<Integer> stations = routeData.stream()
								.skip(1)
								.map(i -> Integer.parseInt(i))
								.collect(Collectors.toCollection(ArrayList<Integer>::new));
		
		routes.add(stations);
	}
}
