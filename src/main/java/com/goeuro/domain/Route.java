package com.goeuro.domain;

/**
 * Class representing the JSON schema for Route
 * @author Deepak
 *
 */
public class Route {
	
	private String dep_sid;
	
	private String arr_sid;
	
	private boolean direct_bus_route;
	
	public Route(String departure, String arrival, boolean isDirectlyConnected) {
		this.setDeparture(departure);
		this.setArrival(arrival);
		this.direct_bus_route = isDirectlyConnected;
	}
	
	public void setDireRouteAvailable(boolean isAvailable) {
		this.direct_bus_route = true;
	}
	
	public boolean getDirect_bus_route() {
		return this.direct_bus_route;
	}

	public String getDep_sid() {
		return dep_sid;
	}

	public void setDeparture(String dep_sid) {
		this.dep_sid = dep_sid;
	}

	public String getArr_sid() {
		return arr_sid;
	}

	public void setArrival(String arr_sid) {
		this.arr_sid = arr_sid;
	}
}
