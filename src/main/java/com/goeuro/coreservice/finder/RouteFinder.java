package com.goeuro.coreservice.finder;

/**
 * Interface for RouteFinder
 * @author Deepak
 *
 */
public interface RouteFinder {
	
	/**
	 * Add routes to query engine
	 * @param routeData - string of stations and bus
	 */
	public void addRoute(String routeData);
	
	/**
	 * Find if there is a direct connection between two stations
	 * @param departure
	 * @param arrival
	 * @return
	 */
	public boolean isDirectlyConnected(String departure, String arrival);

}
