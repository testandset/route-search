package com.goeuro.coreservice.finder;

import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RouteFinderPerformanceTest {

	private static RouteFinder routeFinder;

	public static void main(String[] args) {

		routeFinder = new NaiveRouteFinder();
		test();
		
		routeFinder = new RouteFinderByGraphImpl();
		test();

	}
	
	private static void test() {
		OptionalDouble l = IntStream.range(1, 10).map(i -> getLoadTime()).average();
		OptionalDouble s = IntStream.range(1, 10).map(i -> getSearchTime()).average();;
		
		System.out.println("Average of 10 loads " + l.getAsDouble());
		System.out.println("Average of 10 searches " + s.getAsDouble());

	}

	private static int getLoadTime() {

		long t = System.currentTimeMillis();
		IntStream.rangeClosed(1, 1000).forEach(i -> routeFinder.addRoute(
				new Random().ints(1000, 1, 1000).mapToObj(Integer::toString).collect(Collectors.joining(" "))));

		return (int) (System.currentTimeMillis() - t);
	}
	
	private static int getSearchTime() {
		int a = (int)(Math.random() * 10000);
		int b = (int)(Math.random() * 10000);
		
		long t = System.currentTimeMillis();
		routeFinder.isDirectlyConnected(a+"", b+"");
		return (int) (System.currentTimeMillis() - t);
	}

}
