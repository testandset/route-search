package com.goeuro.rs;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.goeuro.RouteSearchService;
import com.jayway.restassured.RestAssured;

/**
 * Integration test for /api/direct endpoint.
 * 
 * @author Deepak
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RouteSearchService.class)
@WebAppConfiguration
@IntegrationTest("spring.config.location=classpath:/test.properties")
public class DirectRouteServiceIntegrationTest {

	private static final String ROUTE_RESOURCE = "/api/direct";

	@Value("${local.server.port}")
	private int serverPort;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;
	}
	
	@Test
	public void findImmediateNextStop() {
		given()
		.queryParam("dep_sid", 150)
		.queryParam("arr_sid", 5)
		.get(ROUTE_RESOURCE)
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("dep_sid", equalTo("150"))
			.assertThat().body("arr_sid", equalTo("5"))
			.assertThat().body("direct_bus_route", equalTo(true));			
	}
	
	@Test
	public void findTransitiveConnection() {
		given()
		.queryParam("dep_sid", 153)
		.queryParam("arr_sid", 12)
		.get(ROUTE_RESOURCE)
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("direct_bus_route", equalTo(true));			
	}
	
	@Test
	public void noConnection() {
		given()
		.queryParam("dep_sid", 150)
		.queryParam("arr_sid", 138)
		.get(ROUTE_RESOURCE)
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("dep_sid", equalTo("150"))
			.assertThat().body("arr_sid", equalTo("138"))
			.assertThat().body("direct_bus_route", equalTo(false));			
	}

	@Test
	public void nonExistingStations() {
		given()
		.queryParam("dep_sid", 1500)
		.queryParam("arr_sid", 2)
		.get(ROUTE_RESOURCE)
		.then()
			.assertThat().statusCode(200)
			.assertThat().body("direct_bus_route", equalTo(false));			
	}
	
	@Test
	public void badRequest() {
		given()
		.queryParam("dep_sid", "150000000000000")
		.queryParam("arr_sid", 2)
		.get(ROUTE_RESOURCE)
		.then()
			.assertThat().statusCode(400)
			.assertThat().body("error", equalToIgnoringCase("Bad Request"))
			.assertThat().body("message", not(isEmptyOrNullString()));			
	}


}

