package com.goeuro.coreservice.finder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.goeuro.domain.Station;

/**
 * Route finder implementation by Graph. It build a graph and then does a Breadth First Search
 * to find if there is a direct connection between two stations.
 * 
 * It compromises on space to get better performance.
 * 
 * Time complexity
 * 		For add a new route - ~O(1)
 * 		For connection search - O(|V| + |E|)
 * 
 * @author Deepak
 *
 */
public class RouteFinderByGraphImpl implements RouteFinder {

	/**
	 * Route graph with maintain the adjacency matrix. We keep all the stations that can be
	 * reached by a particular station.
	 */
	private Map<Integer, Set<Station>> routeGraph = new HashMap<Integer, Set<Station>>();
	
	/**
	 * List of all the stations processed for quicker search if the stations don't exist	 * 
	 */
	private Set<Integer> busStations = new HashSet<Integer>();	
	
	/**
	 * Implementation of interface method to add routes to engine.
	 * 
	 * @param routeEntry - route entry in text file
	 */
	@Override
	public void addRoute(String routeEntry) {
		List<String> routeData = Arrays.asList(routeEntry.split("\\s+"));
		
		int bus = Integer.parseInt(routeData.get(0));
		
		IntStream.range(1,routeData.size()-1)
			.forEach(i -> addRoute(Integer.parseInt(routeData.get(i)), Integer.parseInt(routeData.get(i+1)), bus));
	}
	
	/**
	 * Convenience method to convert id to Station objects
	 */
	private void addRoute(int from, int to, int viaBus) {
		addRoute(new Station(from), new Station(to, viaBus));
	}


	/**
	 * Implementation of interface method. Checks if there is a direct connection
	 * between two stations.
	 */
	@Override
	public boolean isDirectlyConnected(String departure, String arrival) {
		return isConnected(Integer.parseInt(departure), Integer.parseInt(arrival));
	}

	/**
	 * Internal implementation to add a bus station.
	 * @param station
	 * 
	 */	
	private void addStation(Station station) {
		routeGraph.putIfAbsent(station.getId(), new HashSet<Station>());
		busStations.add(station.getId());
	}
	
	/**
	 * Internal implementation to add a route between two conceptual stations.
	 * @param from
	 * @param to
	 */
	private void addRoute(Station from, Station to) {
		addStation(from);
		addStation(to);
		
		routeGraph.get(from.getId()).add(to);
	}	
	
	/**
	 * Convenience method for integer arguments
	 * @param from
	 * @param to
	 * @return
	 */
	private boolean isConnected(int from, int to) {
		return isConnected(new Station(from), new Station(to));
	}
	
	/**
	 * Breadth first search to find if two stations are served directly.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	private boolean isConnected(Station from, Station to) {
		
		Set<Station> toVisit;
		ArrayList<Station> visited = new ArrayList<Station>();

		// Lets get easy thing out of way, if both of them are same return true;
		if(from.getId() == to.getId()) return true;
		
		// If we don't have info about either of two stations return false
		if(!busStations.contains(from.getId()) || !busStations.contains(to.getId())) return false;
		
		// lets start DFS by getting all immediate connections
		Set<Station> connectedStations = routeGraph.get(from.getId());
		
		// If the arrival station is part of immediate connection, easy win.
		if(connectedStations
				.stream()
				.anyMatch(p -> p.getId() == to.getId())) return true;
		
		// no luck yet, lets go one step deeper
		do {
			
			// find all the other stations that can be visited by same bus
			// and filter out the ones we've already visited.
			
			// Because we dont have to modify any object in search lets also use parallel stream 
			// to split it and do it faster
			toVisit = connectedStations
            		.parallelStream()            		
            		.map(e -> routeGraph.get(e.getId())
            				.parallelStream()
            				.filter(p -> p.getConnectingBus() == e.getConnectingBus() && !visited.contains(p))
            				.collect(Collectors.toSet()))
            		.flatMap(Set<Station>::stream)
            		.collect(Collectors.toSet());
			
			// check if have it
			if(toVisit.stream()
					.anyMatch(p -> p.getId() == to.getId())) return true;
            
			// no luck, rinse and repeat
            visited.addAll(connectedStations);
            connectedStations = toVisit;
            
        } while(!toVisit.isEmpty());
		
		// no direct connection
		return false;		
	}
}
