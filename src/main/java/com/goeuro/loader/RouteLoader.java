package com.goeuro.loader;

/**
 * Interface for Route loader
 * 
 * @author Deepak
 *
 */
public interface RouteLoader {
	
	public void loadRoutes();

}
