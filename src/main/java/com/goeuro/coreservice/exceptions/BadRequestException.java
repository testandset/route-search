package com.goeuro.coreservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for malformed or invalid requests. Sets 400 HTTP Status
 * 
 * @author Deepak
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BadRequestException(String error) {
		super(error);
	}
}
