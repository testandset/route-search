package com.goeuro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Main application class. This is where it begins. Bootstraps application with
 * application context and initiates it.
 * 
 * @author Deepak
 *
 */
@SpringBootApplication
@ImportResource("applicationContext.xml")
public class RouteSearchService {

	public static void main(String[] args) {
		SpringApplication.run(RouteSearchService.class, args);
	}
}