package com.goeuro.rs;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.coreservice.RouteService;
import com.goeuro.coreservice.RouteValidator;
import com.goeuro.domain.Route;

/**
 * Main resource diret API resource. 
 * 
 * Add all future HTTP verbs here for /direct
 * 
 * @author Deepak
 */
@RestController
@RequestMapping("/api/direct")
public class DirectRouteResource {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Resource(name = "routeService")
	private RouteService service;

	@Resource(name = "validator")
	private RouteValidator validator;
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Route> isDirectRouteAvailable(@RequestParam("dep_sid") String departure,
			@RequestParam("arr_sid") String arrival) {

		logger.debug("/GET /direct?dep_sid=" + departure + "&arr_sid=" + arrival);

		validator.validateSid(departure);
		validator.validateSid(arrival);

		return ResponseEntity
				.ok() // status
				.body(service.isDirectRouteAvailable(departure, arrival)); // response
	}

}
