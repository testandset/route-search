package com.goeuro.loader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.goeuro.coreservice.finder.RouteFinder;

/**
 * File file loader implementation for RouteLoader.
 * 
 * Reads a file on File system and build the route engine.
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class TextFileRouteLoader implements RouteLoader{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RouteFinder routeFinder;
	
	@Value("${route.list}")
	private String fileName;

	/**
	 * Load routes specified in a text file.
	 */
	public void loadRoutes(){

		ResourceLoader resourceLoader = new FileSystemResourceLoader();
		Resource resource = resourceLoader.getResource(fileName);
		InputStream file;
		BufferedReader reader = null;

		try {
			file = resource.getInputStream();
			reader = new BufferedReader(new InputStreamReader(file));

			int totalRoutes = Integer.parseInt(reader.readLine());

			// only read totalRoutes lines even if there are more lines in the file
			for (int i = 0; i < totalRoutes; i++) {
				String route = reader.readLine();

				if (route != null) {
					routeFinder.addRoute(route);
				}
			} 
		} catch(IOException e){
			logger.error(e.getMessage());
		}
	}


}
