package com.goeuro;

import javax.annotation.Resource;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.goeuro.loader.RouteLoader;

/**
 * Startup tasks for service. Loads route file.
 * 
 * @author Deepak
 *
 */
@Component
public class RouteServiceStartup implements ApplicationListener<ApplicationReadyEvent> {

	@Resource(name = "routeLoader")
	private RouteLoader routeLoader;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent arg0) {
		routeLoader.loadRoutes();
	}

}
