package com.goeuro.coreservice;

import com.goeuro.domain.Route;

/**
 * Interface for Route service
 * 
 * @author Deepak
 */
public interface RouteService {
	
	public Route isDirectRouteAvailable(String departure, String arrival);

}
