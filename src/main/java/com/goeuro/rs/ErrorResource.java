package com.goeuro.rs;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Custom error representation for business and server errors.
 * 
 * @author Deepak
 */
@RestController
@RequestMapping(value = "/error")
public class ErrorResource implements ErrorController {

	private final ErrorAttributes errorAttributes;

	@Autowired
	public ErrorResource(ErrorAttributes errorAttributes) {
		this.errorAttributes = errorAttributes;
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

	/**
	 * Set custom error attrubutes.
	 * @param aRequest
	 * @return
	 */
	@RequestMapping
	public Map<String, Object> error(HttpServletRequest aRequest) {
		Map<String, Object> errorResponse = getErrorAttributes(aRequest);
		return errorResponse;
	}

	/**
	 * Get all error attributes from http request.
	 * 
	 * @param aRequest
	 * @return 
	 */
	private Map<String, Object> getErrorAttributes(HttpServletRequest aRequest) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(aRequest);
		return errorAttributes.getErrorAttributes(requestAttributes, false);
	}
}
