package com.goeuro.coreservice;

import javax.annotation.Resource;

import com.goeuro.coreservice.finder.RouteFinder;
import com.goeuro.domain.Route;

public class RouteServiceImpl implements RouteService {

	@Resource(name="routeFinder")
	private RouteFinder routeFinder;

	@Override
	public Route isDirectRouteAvailable(String departure, String arrival) {
		return new Route(departure, arrival, routeFinder.isDirectlyConnected(departure, arrival));
	}

}
