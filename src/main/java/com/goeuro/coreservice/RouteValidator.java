package com.goeuro.coreservice;

import org.apache.commons.lang3.StringUtils;

import com.goeuro.coreservice.exceptions.BadRequestException;

/**
 * Request validator
 * 
 * Checks if both inputs are valid 32 bit integers
 * @author Deepak
 *
 */
public class RouteValidator {
	
	public void validateSid(String sid) {
		if(!StringUtils.isNumeric(sid)){
			throw new BadRequestException("Only 32 bit integer values are allowed for deparutre and arrival points");
		}
		
		try {
			 Integer.parseInt(sid);
		} catch (NumberFormatException mfe) {
			throw new BadRequestException("Only 32 bit integer values are allowed for deparutre and arrival points");
		}
	}

}
